/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Header';

export default defineMessages({
  home: {
    id: `${scope}.home`,
    defaultMessage: 'Home',
  },
  features: {
    id: `${scope}.features`,
    defaultMessage: 'Features',
  },
  konvaZero: {
    id: `${scope}.konvaZero`,
    defaultMessage: 'Konva Zero',
  },
  canvas: {
    id: `${scope}.canvas`,
    defaultMessage: 'Canvas',
  },
  radi: {
    id: `${scope}.radi`,
    defaultMessage: 'Radi',
  },
  editarPersonaje: {
    id: `${scope}.editarPersonaje`,
    defaultMessage: 'Editar Personaje',
  },
  editarPagina: {
    id: `${scope}.editarPagina`,
    defaultMessage: 'Editar Pagina',
  },
});
