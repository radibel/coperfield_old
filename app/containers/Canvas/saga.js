/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { RETRIEVE_CUERPOS, RETRIEVE_CUERPOS_SUCCESS } from 'containers/Canvas/constants';
// import { reposLoaded, repoLoadingError } from 'containers/Canvas/actions';
import { categoryCuerposLoaded } from './actions';

import request from 'utils/request';
import { makeSelectUsername } from 'containers/HomePage/selectors';

/**
 * Github repos request/response handler
 */
export function* getCuerpos() {
  console.log("saga aqui / getCuerpos")
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  const allItems_url = "http://192.168.1.103:8080/item/all"

  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, allItems_url);
    // yield put(reposLoaded(repos, username));
    yield put(categoryCuerposLoaded("pene"));
    console.log(result)
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* rootSaga() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(RETRIEVE_CUERPOS, getCuerpos);
  // yield takeEvery(RETRIEVE_CUERPOS, getCuerpos);
}
