/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
// import {
//   RETRIEVE_CUERPOS
//   // RETRIEVE_BRAZOS,  
//   // RETRIEVE_EXPRESIONES,
//   // RETRIEVE_peinados,
//   // SET_SELECTED_CUERPO,
//   // SET_SELECTED_BRAZO,
//   // SET_SELECTED_EXPRESION,
//   // SET_SELECTED_peinado,
//   // SEND_GUARDARPERSONAJE,
// } from './actions';

import { RETRIEVE_CUERPOS, RETRIEVE_CUERPOS_SUCCESS, RETRIEVE_CUERPOS_ERROR } from './constants';

// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  currentUser: false,
  userData: {
    repositories: false,
  },

  brazos: [],
  peinados: [],
  cuerpos: [],
  expresiones: [],
  isFetching: false,
  items: [],
  personajeId: 0,
  nombrePersonaje: '',
  personajeGuardado: [],
};

/* eslint-disable default-case, no-param-reassign */
const personajeReducer = (state = initialState, action) =>
  produce(state, draft => {    
    switch (action.type) {      
      // case LOAD_REPOS:
      //   draft.loading = true;
      //   draft.error = false;
      //   draft.userData.repositories = false;
      //   break;

      // case LOAD_REPOS_SUCCESS:
      //   draft.userData.repositories = action.repos;
      //   draft.loading = false;
      //   draft.currentUser = action.username;
      //   break;

      // case LOAD_REPOS_ERROR:
      //   draft.error = action.error;
      //   draft.loading = false;
      //   break;

      // CUERPOS
      case RETRIEVE_CUERPOS:
          draft.loading = true;
          draft.error = false;
        break;
      case RETRIEVE_CUERPOS_SUCCESS:
        let cuerpoi = 0;
        draft.isFetching = false;
        console.log("asldfkj")
        draft.cuerpos = action.payload.map(item => {
          return {
            ...item,
            selected: false,
            index: cuerpoi++,
          };
        });
        break;

      case RETRIEVE_CUERPOS_ERROR:
            draft.error = action.error;
            draft.loading = false;
        break;

//       case SET_SELECTED_CUERPO:                  
//           draft.cuerpos=setSelectedCuerpo(state.cuerpos, action.payload)
//         break;

//       // BRAZOS
//       case RETRIEVE_BRAZOS.BEGIN:
//           draft.isFetching=true;
//         break;

//       case RETRIEVE_BRAZOS.SUCCESS:
//         let brazoi = 0;
//           draft.isFetching=false
//           draft.brazos=action.payload.map(item => {
//             return {
//               ...item,
//               selected: false,
//               index: brazoi++,
//             };
//           });
//         break;

//       case RETRIEVE_BRAZOS.ERROR:          
//           draft.isFetching=false
//         break;

//       case SET_SELECTED_BRAZO:
//           draft.brazos=setSelectedBrazo(state.brazos, action.payload)
//         break;

//       // EXPRESIONES
//       case RETRIEVE_EXPRESIONES.BEGIN:
//         draft.isFetching=true
//         break;

//       case RETRIEVE_EXPRESIONES.SUCCESS:
//         let expresioni = 0;
//         draft.isFetching=false,
//         draft.expresiones=action.payload.map(item => {
//             return {
//               ...item,
//               selected: false,
//               index: expresioni++,
//             };
//           });
//         break;

//       case RETRIEVE_EXPRESIONES.ERROR:        
//         draft.isFetching=false
//         break;

//       case SET_SELECTED_EXPRESION:        
//         draft.expresiones=setSelectedExpresion(state.expresiones, action.payload)
//         break;

//       // peinados
//       case RETRIEVE_peinados.BEGIN:        
//           isFetching=true
//         break;

//       case RETRIEVE_peinados.SUCCESS:
//         let peinadoi = 0;        
//         draft.isFetching=false
//         draft.peinados=action.payload.map(item => {
//             return {
//               ...item,
//               selected: false,
//               index: peinadoi++,
//             };
//           })
//         break;

//       case RETRIEVE_peinados.ERROR:        
//         draft.isFetching=false
//         break;

//       case SET_SELECTED_peinado:        
//         draft.peinados=setSelectedpeinado(state.peinados, action.payload)
//         break;

//       // GUARDAR PERSONAJE
//       case SEND_GUARDARPERSONAJE.BEGIN:        
//         draft.isFetching=true
//         break;        

//       case SEND_GUARDARPERSONAJE.SUCCESS:                
//         draft.isFetching=false
//         draft.personajeId=action.payload
//         break;

//       case SEND_GUARDARPERSONAJE.ERROR:        
//         draft.isFetching=false
//         break;
    }
  });

//   const setSelectedCuerpo = (items, index) => {
//     items.forEach(element => {
//         element.selected = false
//     });
//     items[index].selected = !items[index].selected;
//     return Object.assign([], items);
// };

// const setSelectedBrazo = (items, index) => {
//     items.forEach(element => {
//         element.selected = false
//     });
//     items[index].selected = !items[index].selected;
//     return Object.assign([], items);
// };

// const setSelectedExpresion = (items, index) => {
//     items.forEach(element => {
//         element.selected = false
//     });
//     items[index].selected = !items[index].selected;
//     return Object.assign([], items);
// };

// const setSelectedpeinado = (items, index) => {
//     items.forEach(element => {
//         element.selected = false
//     });
//     // items.forEach(item => {
//     //     item.id === id ? item.selected = true : item.selected = false
//     // });
//     items[index].selected = !items[index].selected;
//     return Object.assign([], items);
// };

// const setSelectedLocation = (locations, id) => {
//     return locations.map((location) => {
//         location.selected = location.locationId === id;
//         return location;
//     });
// };

export default personajeReducer;
