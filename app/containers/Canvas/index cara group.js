import React from 'react';
import { render } from 'react-dom';
import { Stage, Layer, Group, Text, Transformer } from 'react-konva';
import Rectangle from '../Rectangle'
import Cara from '../Cara'

// reemplazar con el global state
const initialCabezaGroup = [
  { 
    id: 'cabezota',    
    scaleX:.5,
    scaleY:.5,
    x: 400,
    y: 200,    
    offsetX: 400,
    offsetY: 200,    
  }
];

export default function Canvas() {
  const [itemsCabezaGroup, setItems] = React.useState(initialCabezaGroup);
  const [selectedId, selectShape] = React.useState(null);

  return (
    <Stage
      width={window.innerWidth}
      height={window.innerHeight}
      onMouseDown={e => {        
        const clickedOnEmpty = e.target === e.target.getStage();
        if (clickedOnEmpty) {
          selectShape(null);
        }
      }}
    >
      <Layer>
        <Text name="Titulo" text="CREA TU PERSONAJE" x={400} y={100}></Text>
        {itemsCabezaGroup.map((item, i) => {          
                return (
                  <Cara key={item.id}
                        shapeProps={item}
                        isSelected={item.id === selectedId}
                        onSelect={() => {
                          selectShape(item.id);
                        }}
                        onChange={newAttrs => {
                          const itemsArray = itemsCabezaGroup.slice();
                          itemsArray[i] = newAttrs;
                          setItems(itemsArray);
                        }}>
                  </Cara>  
          )}
        )}        
      </Layer>
    </Stage>
  );
};



