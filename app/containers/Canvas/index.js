import React, { Component, memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { render } from 'react-dom';
import { Stage, Layer, Group, Text, Transformer } from 'react-konva';
import Rectangle from '../Rectangle'
// import ShapeNoRotation from '../ShapeNoRotation'
import URLImageTransform from '../URLImageTransform'
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import { createStructuredSelector } from 'reselect';
import { Row, Col, ListGroup, Button } from "react-bootstrap";

//Actions
import {    
  getCategoryCuerpos
  // getCategoryBrazos,
  // getCategoryExpresiones,
  // getCategorypeinados,

  // setSelectCuerpo,
  // setSelectBrazo,
  // setSelectExpresion,
  // setSelectpeinado,
  // guardarPersonaje,
  
} from './actions';
//Selectors
import {
  PersonajeCuerposSelector,
  // PersonajeBrazosSelector,
  // PersonajeExpresionesSelector,
  // PersonajepeinadosSelector,
  // PersonajeCompletoSelector, 
} from './selectors';

// reemplazar con el global state
const initialCabezaGroup = [
  { 
    id: 'cabezota',
    scaleX:.5,
    scaleY:.5,
    x: 400,
    y: 200,
    offsetX: 0,
    offsetY: 0,
  }
];
const initialBrazos = [    
  { 
    id: 'pierna izquierdo',    
    scaleX:.12,
    scaleY:.12,
    offsetX:500,
    offsetY:50,
    x: 480,
    y: 700,  
    url: 'https://i.imgur.com/hMlP9tV.png'
  },
  { 
    id: 'pierna derecho',    
    scaleX:.12,
    scaleY:.12,
    offsetX:500,
    offsetY:50,
    x: 570,
    y: 700,  
    url: 'https://i.imgur.com/c8a6Nba.png'
  },
  { 
    id: 'brazo izquierdo',    
    scaleX:.25,
    scaleY:.25,
    x: 495,
    y: 490,
    offsetX: 500,
    offsetY: 0,
    rotation: 90,
    url: 'https://i.imgur.com/dMJDkcR.png'
  },
  { 
    id: 'brazo derecho',    
    scaleX:.25,
    scaleY:.25,
    x: 555,
    y: 490,
    offsetX: 500,
    offsetY: 0,
    rotation: 270,
    url: 'https://i.imgur.com/iNhToh2.png'
  }
];

export function Canvas({
  // username,
  // loading,
  // error,
  // repos,
  // onSubmitForm,
  onGetCategoryCuerpos  
}) {  
  useInjectReducer({ key: 'radi', reducer });
  useInjectSaga({ key: 'radi', saga });

  const [selectedId, selectShape] = React.useState(null);
  const [itemsCabezaGroup, setItemsCabezaGroup] = React.useState(initialCabezaGroup);
  const [itemsBrazos, setItemsBrazos] = React.useState(initialBrazos);  

  React.useEffect(() => { 
    // console.log("start useEffect")
    const temp = onGetCategoryCuerpos()
  });
  

  return (    
    <Stage
      width={window.innerWidth}
      height={window.innerHeight}
      onMouseDown={e => {
        const clickedOnEmpty = e.target === e.target.getStage();
        if (clickedOnEmpty) {
          selectShape(null);
        }
      }}
    >
      <Layer>
        <Text name="Titulo" text="CREA TU PERSONAJE" x={400} y={100}></Text>
        
        {/* CABEZA CARA Y PELO */}
        {/* {itemsCabezaGroup.map((item, i) => {          
                return (
                  <ShapeNoRotation key={item.id}
                        shapeProps={item}
                  >
                  </ShapeNoRotation>
          )}
        )} */}

         {/* BRAZOS Y PIERNAS */}
          {itemsBrazos.map((item, i) => {
              return (
                      <URLImageTransform
                        key={item.id}
                        shapeProps={item}
                        isSelected={item.id === selectedId}
                        onSelect={() => {
                          selectShape(item.id);                         
                        }}                     
                      />
                    )}
                  )}        
      </Layer>
    </Stage>
  );
};

Canvas.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
// Canvas.propTypes = {
//   // loading: PropTypes.bool,
//   // error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
//   // repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
//   // onSubmitForm: PropTypes.func,
//   // username: PropTypes.string,
//   onGetCategoryCuerpos: PropTypes.func,
// };

const mapStateToProps = createStructuredSelector({
  initialBrazos: PersonajeCuerposSelector(),
  // brazos: PersonajeBrazosSelector(state),
  // expresiones: PersonajeExpresionesSelector(state),
  // peinados: PersonajepeinadosSelector(state),
  // personajeCompleto: PersonajeCompletoSelector(state),
  // repos: makeSelectRepos(),
  // username: makeSelectUsername(),

  // loading: makeSelectLoading(),
  // error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onGetCategoryCuerpos: evt => dispatch(getCategoryCuerpos("pene"))
    // onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    // onSubmitForm: evt => {
    //   if (evt !== undefined && evt.preventDefault) evt.preventDefault();
    //   dispatch(loadRepos());
    // },

  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Canvas);

// import React from 'react';
// import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import { compose } from 'redux';

// export function Personaje() {
//   return <div />;
// }

// Personaje.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// };

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(
//   null,
//   mapDispatchToProps,
// );

// export default compose(withConnect)(Personaje);
