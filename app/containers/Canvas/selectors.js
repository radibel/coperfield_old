/**
 * The global state selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectGlobal = state => state.global || initialState;
// const selectGlobal = state => initialState;

const selectRouter = state => state.router;

const makeSelectCurrentUser = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.currentUser,
  );

const makeSelectLoading = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.loading,
  );

const makeSelectError = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.error,
  );

const makeSelectRepos = () =>
  createSelector(
    selectGlobal,
    globalState => globalState.userData.repositories,
  );

const makeSelectLocation = () =>
  createSelector(
    selectRouter,
    routerState => routerState.location,
  );

export {
  selectGlobal,
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectError,
  makeSelectRepos,
  makeSelectLocation,
};

// export const PersonajeSelector = (state) => {
//   return state.personaje;
// };

export const PersonajeCuerposSelector = () => createSelector(  
  selectGlobal,
  (personaje) => {
    console.log(selectGlobal)
      return personaje;
  }
);

// export const PersonajeBrazosSelector = () => createSelector(
//   [PersonajeSelector],
//   (personaje) => {
//       return personaje.brazos;
//   }
// );

// export const PersonajeExpresionesSelector = () => createSelector(
//   [PersonajeSelector],
//   (personaje) => {
//       return personaje.expresiones;
//   }
// );

// export const PersonajepeinadosSelector = () => createSelector(
//   [PersonajeSelector],
//   (personaje) => {
//       return personaje.peinados;
//   }
// );

// export const PersonajeIsFetchingSelector = () => createSelector(
//   [PersonajeSelector],
//   (personaje) => {
//       return personaje.isFetching;
//   }
// );

// export const PersonajeCompletoSelector = () => createSelector(
//   [PersonajeCuerposSelector,
//    PersonajeBrazosSelector,
//    PersonajeExpresionesSelector,
//    PersonajepeinadosSelector],
//   (cuerpos, brazos, expresiones, peinados) => {
//       let _personaje = { personajeCompleto: [], personajeBrazos: [] }; 

//       cuerpos.forEach((item) => {
//           if (item.selected) {
//               _personaje.personajeCompleto.push(item)
//           }
//       })
//       brazos.forEach((item) => {
//           if (item.selected) {
//               _personaje.personajeBrazos.push(item)
//           }
//       })
//       expresiones.forEach((item) => {
//           if (item.selected) {
//               _personaje.personajeCompleto.push(item)
//           }
//       })
//       peinados.forEach((item) => {
//           if (item.selected) {
//               _personaje.personajeCompleto.push(item)
//           }
//       })
      
//       return _personaje;
//   }
// );
