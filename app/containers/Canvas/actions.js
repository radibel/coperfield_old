/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */
import { apiThunkHelper, apiThunkHelperPost } from './ApiHelper';
import { LOAD_REPOS, LOAD_REPOS_SUCCESS, LOAD_REPOS_ERROR } from './constants';
import { RETRIEVE_CUERPOS, RETRIEVE_CUERPOS_SUCCESS, RETRIEVE_CUERPOS_ERROR } from './constants';

/**
 * Load the repositories, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function loadRepos() {
  return {
    type: LOAD_REPOS,
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of LOAD_REPOS_SUCCESS passing the repos
 */
export function reposLoaded(repos, username) {
  return {
    type: LOAD_REPOS_SUCCESS,
    repos,
    username,
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function repoLoadingError(error) {
  return {
    type: LOAD_REPOS_ERROR,
    error,
  };
}


// CUERPO
// export const SET_SELECTED_CUERPO = 'SET_SELECTED_CUERPO';
// export function setSelectCuerpo(index) {
//     return {
//         payload: index,
//         type: SET_SELECTED_CUERPO
//     };
// }
export function getCategoryCuerpos(body) {
    console.log(body)    
    return {
      type: RETRIEVE_CUERPOS      
    };
  }

export function categoryCuerposLoaded(response) {
    return {
      type: LOAD_REPOS_SUCCESS,
      response      
    };
  }
// export function getCategoryCuerpos(body) {
//   console.log(body)
//   const requestObject = {
//                 Name: 'items',
//                 Path: '/all',
//                 Init: {body}
//                 };
//   const response = apiThunkHelper(RETRIEVE_CUERPOS_SUCCESS, requestObject);  
//   return {
//     type: RETRIEVE_CUERPOS_SUCCESS,
//     response
//   };
// }

// // BRAZOS
// export const RETRIEVE_BRAZOS = {
//     BEGIN: 'RETRIEVE_BRAZOS.BEGIN',
//     FAILURE: 'RETRIEVE_BRAZOS.FAILURE',
//     SUCCESS: 'RETRIEVE_BRAZOS.SUCCESS'
// };

// const RETRIEVE_BRAZOS_TYPES = [
//     RETRIEVE_BRAZOS.BEGIN,
//     RETRIEVE_BRAZOS.SUCCESS,
//     RETRIEVE_BRAZOS.FAILURE
// ];

// export const SET_SELECTED_BRAZO = 'SET_SELECTED_BRAZO';
// export function setSelectBrazo(index) {
//     return {
//         payload: index,
//         type: SET_SELECTED_BRAZO
//     };
// }

// export function getCategoryBrazos(body) {    
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'items',
//             Path: '/all',
//             Init: {body}
//         };        
//         return apiThunkHelper(dispatch, RETRIEVE_BRAZOS_TYPES, requestObject);
//     };
// }

// // EXPRESIONES
// export const RETRIEVE_EXPRESIONES = {
//     BEGIN: 'RETRIEVE_EXPRESIONES.BEGIN',
//     FAILURE: 'RETRIEVE_EXPRESIONES.FAILURE',
//     SUCCESS: 'RETRIEVE_EXPRESIONES.SUCCESS'
// };

// const RETRIEVE_EXPRESIONES_TYPES = [
//     RETRIEVE_EXPRESIONES.BEGIN,
//     RETRIEVE_EXPRESIONES.SUCCESS,
//     RETRIEVE_EXPRESIONES.FAILURE
// ];

// export const SET_SELECTED_EXPRESION = 'SET_SELECTED_EXPRESION';
// export function setSelectExpresion(index) {
//     return {
//         payload: index,
//         type: SET_SELECTED_EXPRESION
//     };
// }

// export function getCategoryExpresiones(body) {    
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'items',
//             Path: '/all',
//             Init: {body}
//         };        
//         return apiThunkHelper(dispatch, RETRIEVE_EXPRESIONES_TYPES, requestObject);
//     };
// }

// export const RETRIEVE_peinados = {
//     BEGIN: 'RETRIEVE_peinados.BEGIN',
//     FAILURE: 'RETRIEVE_peinados.FAILURE',
//     SUCCESS: 'RETRIEVE_peinados.SUCCESS'
// };

// const RETRIEVE_peinados_TYPES = [
//     RETRIEVE_peinados.BEGIN,
//     RETRIEVE_peinados.SUCCESS,
//     RETRIEVE_peinados.FAILURE
// ];

// export const SET_SELECTED_peinado = 'SET_SELECTED_peinado';
// export function setSelectpeinado(index) {
//     return {
//         payload: index,
//         type: SET_SELECTED_peinado
//     };
// }

// export function getCategorypeinados(body) {    
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'items',
//             Path: '/all',
//             Init: {body}
//         };        
//         return apiThunkHelper(dispatch, RETRIEVE_peinados_TYPES, requestObject);
//     };
// }

// // TODOS
// export function getItemsAll(body) {    
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'items',
//             Path: '/all',
//             Init: {body}
//         };        
//         return apiThunkHelper(dispatch, RETRIEVE_peinados_TYPES, requestObject);
//     };
// }


// // GUARDAR PERSONAJE
// export const SEND_GUARDARPERSONAJE = {
//     BEGIN: 'SEND_GUARDARPERSONAJE.BEGIN',
//     FAILURE: 'SEND_GUARDARPERSONAJE.FAILURE',
//     SUCCESS: 'SEND_GUARDARPERSONAJE.SUCCESS'
// };

// const SEND_GUARDARPERSONAJE_TYPES = [
//     SEND_GUARDARPERSONAJE.BEGIN,
//     SEND_GUARDARPERSONAJE.SUCCESS,
//     SEND_GUARDARPERSONAJE.FAILURE
// ];

// export function guardarPersonaje(body) {    
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'element/design/add/wrapper',
//             Path: '/personaje',
//             Init: {body}
//         };        
//         return apiThunkHelperPost(dispatch, SEND_GUARDARPERSONAJE_TYPES, requestObject);
//     };
// }





// export const RETRIEVE_BILLS = {
//     BEGIN: 'RETRIEVE_BILLS.BEGIN',
//     FAILURE: 'RETRIEVE_BILLS.FAILURE',
//     SUCCESS: 'RETRIEVE_BILLS.SUCCESS'
// };

// const RETRIEVE_BILLS_TYPES = [
//     RETRIEVE_BILLS.BEGIN,
//     RETRIEVE_BILLS.SUCCESS,
//     RETRIEVE_BILLS.FAILURE
// ];

// export function getBills(locationId) {
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'bills',
//             Path: `/bills/${locationId}`,
//             Init: {}
//         };
//         return apiThunkHelper(dispatch, RETRIEVE_BILLS_TYPES, requestObject);
//     };
// }


// export const SET_SELECTED_LOCATION = 'SET_SELECTED_LOCATION';
// export function setSelectedLocation(id) {
//     return {
//         payload: id,
//         type: SET_SELECTED_LOCATION
//     };
// }



// export const SET_SELECTED_ALL_BILLS = 'SET_SELECTED_ALL_BILLS';
// export function setSelectedAllBills(selected) {
//     return {
//         payload: selected,
//         type: SET_SELECTED_ALL_BILLS
//     };
// }