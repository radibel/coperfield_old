/*
 *
 * Personaje constants
 *
 */

export const DEFAULT_ACTION = 'app/Radi/DEFAULT_ACTION';

export const LOAD_PERSONAJE = 'boilerplate/Radi/LOAD_PERSONAJE';
export const LOAD_REPOS_SUCCESS = 'boilerplate/Radi/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS_ERROR = 'boilerplate/Radi/LOAD_REPOS_ERROR';


export const SAVE_ACTION = 'app/Radi/SAVE_ACTION';
