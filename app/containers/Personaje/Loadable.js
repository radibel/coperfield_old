/**
 *
 * Asynchronously loads the component for Personaje
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
