/*
 *
 * Personaje actions
 *
 */

import { DEFAULT_ACTION, SAVE_ACTION } from './constants';
import { LOAD_PERSONAJE, LOAD_REPOS_SUCCESS, LOAD_REPOS_ERROR } from './constants';

export function defaultAction() {
  console.log("default action")
  return {
    type: DEFAULT_ACTION
  };
}

export function loadAll(items) {
  console.log("load action")
  return {
    type: LOAD_PERSONAJE,
    items
  };
}

export function saveShapeAction(attrs) {
  console.log("save action")
  return {
    type: SAVE_ACTION,
    attrs
  };
}

// export function reposLoaded(body, username) {
//   return {
//     type: LOAD_REPOS_SUCCESS,
//     body,
//     username,
//   };
// }

// export function repoLoadingError(error) {
//   return {
//     type: LOAD_REPOS_ERROR,
//     error,
//   };
// }
