import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the personaje state domain
 */

const selectPersonajeDomain = state => state.personaje || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Personaje
 */

const makeSelectPersonaje = () =>
  createSelector(
    selectPersonajeDomain,
    substate => substate,
  );

export default makeSelectPersonaje;
export { selectPersonajeDomain };
