import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPersonaje from './selectors';
import makeSelectEditarPersonaje from '../EditarPersonaje/selectors';

import { defaultAction, saveShapeAction } from './actions';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { isEmpty, isNull } from 'lodash';

import { Stage, Layer, Group, Text, Transformer } from 'react-konva';
import URLImageTransform from '../URLImageTransform';
import URLImageFijo from '../URLImageFijo';

export function Personaje({ personaje, getPersonajeDefault, editarPersonaje }) {
  useInjectReducer({ key: 'personaje', reducer });
  useInjectSaga({ key: 'personaje', saga });

  const cabezotaRef = React.useRef();
  const [selectedId, selectShape] = React.useState(null);

  React.useEffect(() => {
    console.log('PERSONAJE');
  }, []);

  return (
    <div>
      <Stage
        width={window.innerWidth}
        height={window.innerHeight}
        onMouseDown={e => {
          const clickedOnEmpty = e.target === e.target.getStage();
          if (clickedOnEmpty) {
            selectShape(null);
          }
        }}
      >
        <Layer>
          <Text name="Titulo" text={'CREA TU PERSONAJE'} x={400} y={100} />

          <Text name="GUARDAR" text={'GUARDAR'} x={0} y={50} />

        {/* CABEZA, CARA Y PEINADO */}
        {editarPersonaje.items.map((item, i) => {
                console.log(item, 'item loco')
                    if (
                      item.category === 3 ||
                      item.category === 4 ||
                      (item.category === 11)
                    ) {
                      return <URLImageFijo key={i++} shapeProps={item} />;
                    }
                  })}

          {/* CABEZA CARA Y PELO */}
          {editarPersonaje.items.map((itemCabezaGroup, i) => {
            if (itemCabezaGroup.category === 10 && itemCabezaGroup.selected) {

              
              {/* return (
                <Group
                  key={itemCabezaGroup.id}
                  id={itemCabezaGroup.id}
                  name="cabezota"
                  className="canvas"
                  {...itemCabezaGroup}
                  ref={cabezotaRef}
                >
                  
                </Group>
              ); */}
            }
          })}

          {/* CUERPO */}
          {editarPersonaje.items.map((item, i) => {
            if (item.category === 1 && item.selected) {
              return <URLImageFijo key={i++} shapeProps={item} />;
            }
          })}

          {/* BRAZOS Y PIERNAS */}
          {editarPersonaje.items.map((item, i) => {
            if (
              item.category === 9 ||
              item.category === 2 ||
              item.category === 12 ||
              (item.category === 13 && item.selected)
            ) {
              return (
                <URLImageTransform
                  key={item.id}
                  shapeProps={item}
                  isSelected={item.id === selectedId}
                  onSelect={() => {
                    selectShape(item.id);
                  }}
                  onSave={attrs => {
                    // let temp = transformToSave(attrs, itemsBrazosPiernas)
                    // setItemsBrazosPiernas(temp)
                    // onSaveShape(attrs);
                  }}
                />
              );
            }
          })}
        </Layer>
      </Stage>
    </div>
  );
}

function transformToSave(attrs, itemsBrazosPiernas) {
  let temp = [];
  itemsBrazosPiernas.forEach(element => {
    if (element.id == attrs.id) {
      temp.push(attrs);
    } else {
      temp.push(element);
    }
  });
  return temp;
}

Personaje.propTypes = {
  dispatch: PropTypes.func.isRequired,
  personaje: PropTypes.object,
  editarPersonaje: PropTypes.object,
  getPersonajeDefault: PropTypes.func,
  onSaveShape: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  personaje: makeSelectPersonaje(),
  editarPersonaje: makeSelectEditarPersonaje(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPersonajeDefault: evt => dispatch(defaultAction()),
    onSaveShape: evt => dispatch(saveShapeAction(evt.target.value)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Personaje);
