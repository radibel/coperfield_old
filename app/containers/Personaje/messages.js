/*
 * Personaje Messages
 *
 * This contains all the text for the Personaje container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Personaje';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Personaje container!',
  },
});
