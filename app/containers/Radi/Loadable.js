/**
 *
 * Asynchronously loads the component for Radi
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
