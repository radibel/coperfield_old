import { takeLatest, take, call, put, select } from 'redux-saga/effects';
import { DEFAULT_ACTION, SAVE_ACTION } from 'containers/Radi/constants';
import { defaultAction, loadAll } from 'containers/Radi/actions';
import request from 'utils/request';

import { LOAD_REPOS } from 'containers/Radi/constants';
import { reposLoaded, repoLoadingError } from 'containers/Radi/actions';

// Individual exports for testing
export default function* radiSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(DEFAULT_ACTION, getCuerpos);
  yield takeLatest(SAVE_ACTION, saveShape);
  // yield takeLatest(LOAD_PERSONAJE, getCuerpos);
}

export function* getCuerpos() {
  // Select username from store
  // const username = yield select(makeSelectUsername());
  // const username = '@anai.dominick'
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  console.log("saga")
  // const requestURL = "http://192.168.1.103:8080/item/all"
//   cuerpos
const requestURL = "http://localhost:8080/item/get/category/id/1"
//   BRAZOS
  // const requestURL = "http://localhost:8080/item/get/category/id/2"
// expresion
  const byCatItems_url3 = "http://localhost:8080/item/get/category/id/3"
//   peinados
  const byCatItems_url4 = "http://localhost:8080/item/get/category/id/4"

  const personajeCompleto = [  
    { 
      category: 10,
      id: 'Cabeza Group',
      scaleX:.5,
      scaleY:.5,
      x: 400,
      y: 200,
      offsetX: 0,
      offsetY: 0,
    },  
    { 
      category: 11,
      id: 'cara contorno',          
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,        
      url: 'https://i.imgur.com/lh7rUC8.png',
      selected: false,
    },
    { 
      category: 1,
      id: "cuerpo",
      scaleX:".25",
      scaleY:".25",
      x: 400,
      y: 450,
      url: "https://i.imgur.com/QH0R7sH.png",
      selected: false,
    },
    { 
      category: 3,
      id: 'expresion',    
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,
      selected: false,
      //url: 'https://i.imgur.com/CoQcC6O.png'
      //url: 'https://i.imgur.com/547XUoP.png'
      // url: 'https://i.imgur.com/iOYMC9G.png'
       url: 'https://i.imgur.com/0goTsaU.png'
       
    },
    { 
      category: 4,
      id: 'peinado',    
      scaleX:.68,
      scaleY:.68,
      x: -90,
      y: -100,  
      selected: false,
      url: 'https://i.imgur.com/H2b0kbf.png'
      //url: 'https://i.imgur.com/T7L0JhE.png'
      //url: 'https://i.imgur.com/m6L31Hx.png'
    },
    { 
      category: 9,
      id: 'pierna izquierdo',    
      scaleX:.12,
      scaleY:.12,
      offsetX:500,
      offsetY:50,
      x: 480,
      y: 700,  
      selected: false,
      url: 'https://i.imgur.com/hMlP9tV.png'
    },
    { 
      category: 9,
      id: 'pierna derecho',    
      scaleX:.12,
      scaleY:.12,
      offsetX:500,
      offsetY:50,
      x: 570,
      y: 700,  
      selected: false,
      url: 'https://i.imgur.com/c8a6Nba.png'
    },
    { 
      category: 2,
      id: 'brazo izquierdo',    
      scaleX:.25,
      scaleY:.25,
      x: 495,
      y: 490,
      offsetX: 500,
      offsetY: 0,
      rotation: 90,
      selected: false,
      url: 'https://i.imgur.com/dMJDkcR.png'
    },
    { 
      category: 2,
      id: 'brazo derecho',    
      scaleX:.25,
      scaleY:.25,
      x: 555,
      y: 490,
      offsetX: 500,
      offsetY: 0,
      rotation: 270,
      selected: false,
      url: 'https://i.imgur.com/iNhToh2.png'
    }
  ];

  try {
    // Call our request helper (see 'utils/request')
    // const items = yield call(request, requestURL);
    // const items = JSON.stringify(personajeCompleto)
    const items = personajeCompleto
        
    yield put(loadAll(items));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }  
}

export function* saveShape() {
  // Select username from store
  // const username = yield select(makeSelectUsername());
  // const username = '@anai.dominick'
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  console.log("saga save")
  const requestURL = "http://192.168.1.103:8080/item/all"

  try {
    
    //SAVE SHAPE HERE
    const result = yield call(request, requestURL);    
    // yield put(loadAll(items));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }
}
