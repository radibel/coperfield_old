/*
 *
 * Radi reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, LOAD_PERSONAJE } from './constants';
import { element } from 'prop-types';

export const initialState = {
  loading: false,
  error: false,  
  brazos: [],
  peinados: [],
  cuerpos: [],
  expresiones: [],  
  items: [],
  personajeId: 0,
  nombrePersonaje: '',
  personajeGuardado: [],
};

/* eslint-disable default-case, no-param-reassign */
const radiReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:      
        draft.loading = false;        
        break;
      case LOAD_PERSONAJE:
        draft.loading = false;
        console.log("reducer LOAD_PERSONAJE")
        console.log(action.items)        
        let elements = []
        action.items.forEach(element => {                   
          elements.push(element)
        });        
        
        elements.forEach(element => {                       
          element.scaleX = parseFloat(element.scaleX) 
          element.scaleY = parseFloat(element.scaleY) 
        });        
        
        draft.items = elements
        break;   
     }
  });

export default radiReducer;
