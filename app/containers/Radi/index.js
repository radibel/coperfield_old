import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectRadi from './selectors';
import makeSelectEditarPersonaje from '../EditarPersonaje/selectors';
import reducer from './reducer';
import saga from './saga';

import { defaultAction, saveShapeAction } from './actions';

import { Stage, Layer, Group, Text, Transformer } from 'react-konva';
import URLImageTransform from '../URLImageTransform'
import URLImageFijo from '../URLImageFijo'
import { isEmpty, isNull } from 'lodash';

// reemplazar con el global state

const initialCabeza = [  
  { 
    id: 'cabeza',    
    scaleX:.5,
    scaleY:.5,
    x: 0,
    y: 0,        
    url: 'https://i.imgur.com/lh7rUC8.png'
  },
  { 
    id: 'cara',    
    scaleX:.5,
    scaleY:.5,
    x: 0,
    y: 0,
    //url: 'https://i.imgur.com/CoQcC6O.png'
    //url: 'https://i.imgur.com/547XUoP.png'
    // url: 'https://i.imgur.com/iOYMC9G.png'
     url: 'https://i.imgur.com/0goTsaU.png'
  },
  // { 
  //   id: 'cuerpo',    
  //   scaleX:.5,
  //   scaleY:.5,
  //   x: 0,
  //   y: 500,
  //   // url: 'https://i.imgur.com/QH0R7sH.png',
  //   url: 'https://i.imgur.com/uU7aPlX.png'
  // },
  { 
    id: 'peinado',    
    scaleX:.68,
    scaleY:.68,
    x: -90,
    y: -100,  
    url: 'https://i.imgur.com/H2b0kbf.png'
    //url: 'https://i.imgur.com/T7L0JhE.png'
    //url: 'https://i.imgur.com/m6L31Hx.png'
  }
];

const initialCuerpo = [ 
  { 
    id: 'cuerpo',    
    scaleX:.25,
    scaleY:.25,
    x: 400,
    y: 450,
    url: 'https://i.imgur.com/QH0R7sH.png',
    // url: 'https://i.imgur.com/uU7aPlX.png'
  },
];

const initialCabezaGroup = [
  { 
    id: 'cabezota',
    scaleX:.5,
    scaleY:.5,
    x: 400,
    y: 200,
    offsetX: 0,
    offsetY: 0,
  }
];

const initialBrazosPiernas = [    
  { 
    id: 'pierna izquierdo',    
    scaleX:.12,
    scaleY:.12,
    offsetX:500,
    offsetY:50,
    x: 480,
    y: 700,  
    url: 'https://i.imgur.com/hMlP9tV.png'
  },
  { 
    id: 'pierna derecho',    
    scaleX:.12,
    scaleY:.12,
    offsetX:500,
    offsetY:50,
    x: 570,
    y: 700,  
    url: 'https://i.imgur.com/c8a6Nba.png'
  },
  { 
    id: 'brazo izquierdo',    
    scaleX:.25,
    scaleY:.25,
    x: 495,
    y: 490,
    offsetX: 500,
    offsetY: 0,
    rotation: 90,
    url: 'https://i.imgur.com/dMJDkcR.png'
  },
  { 
    id: 'brazo derecho',    
    scaleX:.25,
    scaleY:.25,
    x: 555,
    y: 490,
    offsetX: 500,
    offsetY: 0,
    rotation: 270,
    url: 'https://i.imgur.com/iNhToh2.png'
  }
];


export function Radi({
  radi,
  getPersonajeDefault,
  itemsCuerpos
}) {
  useInjectReducer({ key: 'radi', reducer });
  useInjectSaga({ key: 'radi', saga });

  const [selectedId, selectShape] = React.useState(null);
  const [itemsCabezaGroup, setItemsCabezaGroup] = React.useState(initialCabezaGroup);
  const [itemsBrazosPiernas, setItemsBrazosPiernas] = React.useState(null);
  // const [itemsCuerpo, setItemsCuerpo] = React.useState(null);

  React.useEffect(() => { 
    if (isEmpty(radi.items)) {
      getPersonajeDefault()    
    }
  });

  return (
  <div>
    <Stage
      width={window.innerWidth}
      height={window.innerHeight}
      onMouseDown={e => {
        const clickedOnEmpty = e.target === e.target.getStage();
        if (clickedOnEmpty) {
          selectShape(null);
        }
      }}
    >
      <Layer>
        <Text name="Titulo" text={"CREA TU PERSONAJE"} x={400} y={100}></Text>        

        <Text name="GUARDAR" text={"GUARDAR"} x={400} y={150}></Text>        
                        
        
        {/* CUERPO*/}
        {itemsCuerpos.items ? itemsCuerpos.items.map((item, i) => {          
              return (
                item.selected ?
                      <URLImageFijo
                      key={item.id}
                      shapeProps={item}
                      />
                      : <Text key={item.id}></Text> )}
                  ) : <Text></Text>}

         {/* BRAZOS Y PIERNAS */}
          {/* {radi.items.map((item, i) => {            
              return (
                      <URLImageTransform
                        key={item.id}
                        shapeProps={item}
                        isSelected={item.id === selectedId}
                        onSelect={() => {
                          selectShape(item.id);                         
                        }}
                        onSave={(attrs) => {   
                          let temp = transformToSave(attrs, itemsBrazosPiernas)
                          setItemsBrazosPiernas(temp) 
                          // onSaveShape(attrs);
                        }}                        
                      />
                    )}
                  )}   */}

               
      </Layer>
    </Stage>
  </div>
  );
}

function transformToSave(attrs, itemsBrazosPiernas) {  
  let temp = []  
  itemsBrazosPiernas.forEach(element => {
    if(element.id == attrs.id) {      
      temp.push(attrs)
    }else{
      temp.push(element)
    }  
  });  
  return temp
}

Radi.propTypes = {
  dispatch: PropTypes.func.isRequired,
  radi: PropTypes.object,
  getPersonajeDefault: PropTypes.func,
  onSaveShape: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  radi: makeSelectRadi(),
  itemsCuerpos: makeSelectEditarPersonaje()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPersonajeDefault: evt => dispatch(defaultAction()),
    onSaveShape: evt => dispatch(saveShapeAction(evt.target.value)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Radi);
