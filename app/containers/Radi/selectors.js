import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the radi state domain
 */

const selectRadiDomain = state => state.radi || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Radi
 */

const makeSelectRadi = () =>
  createSelector(
    selectRadiDomain,
    substate => substate,
  );

export default makeSelectRadi;
export { selectRadiDomain };
