import React from 'react'
import { Stage, Layer, Rect, Transformer } from 'react-konva';


const Rectangle = ({ shapeProps, isSelected, onSelect, onChange }) => {    
    const shapeRef = React.useRef();
    const trRef = React.useRef();      
  
    React.useEffect(() => {      
      if (isSelected) {        
        trRef.current.setNode(shapeRef.current);
        trRef.current.getLayer().batchDraw();        
      }          

    }, [isSelected]); 


    return (
      <React.Fragment>
        <Rect
          offsetY={15}
          onClick={onSelect}
          ref={shapeRef}
          {...shapeProps}
          onTransform={e => {
            const node = shapeRef.current;          
            node.position({x:500, y: 300})
          }}          
          onTransformEnd={e => {
            // transformer is changing scale of the node
            // and NOT its width or height
            // but in the store we have only width and height
            // to match the data better we will reset scale on transform end
            const node = shapeRef.current;
            const scaleX = node.scaleX();
            const scaleY = node.scaleY();

            // we will reset it back
            node.scaleX(1);
            node.scaleY(1);                        
     
            onChange({
              ...shapeProps,
              x: node.x(),
              y: node.y(),              
              // set minimal value
              width: Math.max(5, node.width() * scaleX),
              height: Math.max(node.height() * scaleY)              
            });
          }}
        />
        {isSelected && (
          <Transformer          
            rotateAnchorOffset={100}
            resizeEnabled={false}
            anchorCornerRadius={4}            
            ref={trRef}
            boundBoxFunc={(oldBox, newBox) => {
              // limit resize
              if (newBox.width < 5 || newBox.height < 5) {
                return oldBox;
              }
              return newBox;
            }}
          />
        )}
      </React.Fragment>
    );
  };

  export default Rectangle

// You have to decide exactly where the rotation point should be moved to, and this depends on the shape you have drawn. 
// A good starting point is usually to use the centre of the rectangle surrounding the shape.

// var dX = node.width()/2;
// var dY = node.height()/2;
// node.offset(dX, dY)

// Note that changing the offset will move the shape so you need to re-position the shape with

// node.position({x:node.x() + dX, y: node.y() + dY})

// onDragEnd={e => {
//   onChange({
//     ...shapeProps,
//     x: e.target.x(),
//     y: e.target.y()              
//   });
// }}




// ---ROTATE ARROUND CENTER WITHOUT AFFECTING THE OFFSETS---
// const degToRad = Math.PI / 180

// const rotatePoint = ({x, y}, deg) => {
//     const rcos = Math.cos(deg * degToRad), rsin = Math.sin(deg * degToRad)
//     return {x: x*rcos - y*rsin, y: y*rcos + x*rsin}
// }

// //current rotation origin (0, 0) relative to desired origin - center (node.width()/2, node.height()/2)
// const topLeft = {x:-node.width()/2, y:-node.height()/2}
// const current = rotatePoint(topLeft, node.rotation())
// const rotated = rotatePoint(topLeft, rotation)
// const dx = rotated.x - current.x, dy = rotated.y - current.y

// node.rotation(rotation)
// node.x(node.x() + dx)
// node.y(node.y() + dy)