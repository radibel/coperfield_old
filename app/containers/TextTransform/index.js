import React from 'react'
import { Image, Transformer, Text } from 'react-konva';
import useImage from 'use-image';

const TextTranform = ({ shapeProps, isSelected, onSelect, onSave, stage, layer }) => {    
    const [props, setProps] = React.useState(shapeProps);    

    const textRef = React.useRef();
    const trRef = React.useRef();

    React.useEffect(() => {      
      if (isSelected) {        
        trRef.current.setNode(textRef.current);
        trRef.current.getLayer().batchDraw();
      }
    }, [isSelected]);

    return (
      <React.Fragment>
        <Text          
          onClick={onSelect}
          draggable={true}
          {...props}
          ref={textRef}                        
          onTransformEnd={e => {            
            // onSave(textRef.current.attrs)
          }}
          onDblClick={e => {            
            // create textarea over canvas with absolute position
            // first we need to find position for textarea
            // how to find it?

            // at first lets find position of text node relative to the stage:
            var textPosition = textRef.current.getAbsolutePosition();

            // then lets find position of stage container on the page:
            var stageBox = stage.current.container().getBoundingClientRect();

            // so position of textarea will be the sum of positions above:
            console.log(stageBox.left)
            console.log(textPosition.x)
            console.log(stageBox.top)
            console.log(textPosition.y)

            var areaPosition = {
              x: stageBox.left + textPosition.x,
              y: stageBox.top + textPosition.y,
            };

            textRef.current.hide(); 
            trRef.current.hide();
            layer.current.draw();

            // create textarea and style it
            var textarea = document.createElement('textarea');
            document.body.appendChild(textarea);

            textarea.value = textRef.current.text();
            textarea.style.position = 'absolute';
            textarea.style.top = areaPosition.y + 'px';
            textarea.style.left = areaPosition.x + 'px';
            textarea.style.width = textRef.current.width() + 10;

            textarea.focus();        

            textarea.addEventListener('keydown', function (e) {              
            // hide on enter
              if (e.key === "Enter") {
                textRef.current.show();                
                textRef.current.text(textarea.value);
                trRef.current.show();
                isSelected = true;
                layer.current.draw();
                document.body.removeChild(textarea);
              }
            });
          }}
        />           
    {isSelected && (
          <Transformer            
            rotateAnchorOffset={50}            
            anchorSize={10}
            anchorFill={"pink"}
            anchorStroke={"black"}            
            borderEnabled={true}
            resizeEnabled={true}
            anchorCornerRadius={30}            
            ref={trRef}
            boundBoxFunc={(oldBox, newBox) => {
              // limit resize
              if (newBox.width < 5 || newBox.height < 5) {
                return oldBox;
              }
              return newBox;
            }}
          />
        )}
      </React.Fragment>
    );
  };

  export default TextTranform
