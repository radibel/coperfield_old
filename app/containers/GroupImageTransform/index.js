import React from 'react';
import { Image, Transformer, Group } from 'react-konva';
import useImage from 'use-image';
import URLImageFijo from '../URLImageFijo';

const GroupImageTransform = ({ shapeProps, isSelected, onSelect, onSave }) => {
  const [props, setProps] = React.useState(shapeProps);

  const shapeRef = React.useRef();
  const trRef = React.useRef();

  const [selectedId, selectShape] = React.useState(null);

  React.useEffect(() => {
    if (isSelected) {
      trRef.current.setNode(shapeRef.current);
      trRef.current.getLayer().batchDraw();
    }
  }, [isSelected]);

  return (
    <React.Fragment>
      {shapeProps.map((item, i) => {
        if (item.category === 10) {
          return (
            <Group
              key={item.id}
              id={item.id}
              name="cabezota"
              className="canvas"
              onClick={onSelect}
              {...item}
              ref={shapeRef}
              // onTransform={e => {
              //   const node = shapeRef.current;
              //   node.position({x:props.x, y: props.y})
              // }}
              // onTransformEnd={e => {
              //   // onSave(shapeRef.current.attrs)
              // }}
              draggable={true}
              isSelected={item.id === selectedId}
              onSelect={() => {
                selectShape(item.id);
              }}
              onSave={attrs => {
                // let temp = transformToSave(attrs, itemsBrazosPiernas)
                // setItemsBrazosPiernas(temp)
                // onSaveShape(attrs);
              }}
            >
              {shapeProps.map((itemTodoLoDemas, i) => {
                if (itemTodoLoDemas.category != 10) {
                  return (
                    <URLImageFijo key={i++} shapeProps={itemTodoLoDemas} />
                  );
                }
              })}
            </Group>
          );
        }
      })}
      {isSelected && (
        <Transformer
          rotateAnchorOffset={50}
          anchorSize={20}
          anchorFill={'pink'}
          anchorStroke={'black'}
          borderEnabled={true}
          resizeEnabled={true}
          anchorCornerRadius={30}
          ref={trRef}
          boundBoxFunc={(oldBox, newBox) => {
            // limit resize
            if (newBox.width < 5 || newBox.height < 5) {
              return oldBox;
            }
            return newBox;
          }}
        />
      )}
    </React.Fragment>
  );
};

export default GroupImageTransform;
