/*
 *
 * EditarPagina reducer
 *
 */
import produce from 'immer';
import { DEFAULT_ACTION, GETPAGINA_ACTION, LOAD_PERSONAJES, LOAD_GALERIA } from './constants';

export const initialState = {
  loading: false,
  error: false,    
  personajes: [],
  personajeGuardado: [],
  libro: "uno",
  pagina: 1,
  usuarioId: 1,
  galeria: [],
};

/* eslint-disable default-case, no-param-reassign */
const editarPaginaReducer = (state = initialState, action) =>
  produce(state, draft => {
    let elements = []
    let elementsPersonaje = []
    switch (action.type) {
      case DEFAULT_ACTION:      
        draft.loading = false;        
        break;       
      case LOAD_PERSONAJES:
        draft.loading = true;
        //console.log(action.personajes[0].detalle)
        action.personajes.forEach(element => {                   
          elements.push(element)
        });        
        
        elements.forEach(element => {    
          element.forEach(item => {                       
            item.scaleX = parseFloat(item.scaleX) 
            item.scaleY = parseFloat(item.scaleY) 
          });                                             
        });                
        draft.personajes = elements
        draft.loading = false;
        break;  
      case LOAD_GALERIA:        
        draft.loading = true;               
        action.galeria.forEach(element => {                   
          elements.push(element)
        });        
        
        elements.forEach(item => {          
            item.scaleX = parseFloat(item.scaleX) 
            item.scaleY = parseFloat(item.scaleY)           
        });                
        draft.galeria = elements
        draft.loading = false;
        break;  
    }
});

export default editarPaginaReducer;
