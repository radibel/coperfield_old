/**
 *
 * Asynchronously loads the component for EditarPagina
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
