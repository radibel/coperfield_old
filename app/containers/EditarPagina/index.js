import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectEditarPagina from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { getPaginaAction, getGaleriaAction } from './actions';

import { Stage, Layer, Group, Text, Transformer, Image } from 'react-konva';
import URLImageTransformObjeto from '../URLImageTransformObjeto'
import TextTransform from '../TextTransform'
import URLImageFijo from '../URLImageFijo'
import GroupImageTransform from '../GroupImageTransform'

import useImage from 'use-image';
import Personaje from '../Personaje'
import { forEach } from 'lodash';

export function EditarPagina({
  editarPagina,  
  getPaginaLocal,
  getGaleriaLocal
}) {
  useInjectReducer({ key: 'editarPagina', reducer });
  useInjectSaga({ key: 'editarPagina', saga });

  
  const [objectSelectedId, selectObjectShape] = React.useState(null);
  const [textSelectedId, selectTextShape] = React.useState(null);
  const [personajeSelectedId, selectPersonajeShape] = React.useState(null);
  
  const dragUrl = React.useRef();
  const dragText = React.useRef();
  const dragDropHelper = React.useRef();

  const stageRef = React.useRef();
  const layerRef = React.useRef();
  
  const [images, setImages] = React.useState([]);
  const [textos, setTextos] = React.useState([]);

  

  React.useEffect(() => {
        
    getPaginaLocal();
    getGaleriaLocal();    
  }, []); 

  return (    
    <div>
      Drag and Drop the image into the Stage:
      <br />
      {editarPagina.galeria.map((item, i) => { 
        return (
            <img   
            width={100} 
            height={100}                      
            key={i++}
            src={item.url}            
            draggable="true"
            onDragStart={e => {
              dragUrl.current = e.target.src;
              dragDropHelper.current = e.currentTarget;
            }}
          />
          )
        })
      }           
      <p              
        draggable="true"
        onDragStart={e => {          
          dragText.current = e.target.text;
          dragDropHelper.current = e.currentTarget;
          }}
        >
        Drag THIS text
      </p>

      <div
        onDrop={e => {          
          // register event position
          stageRef.current.setPointersPositions(e); 
          let fullProperties

          if(dragDropHelper.current.tagName === "IMG")
          {            
            editarPagina.galeria.forEach(item => {
              if(dragUrl.current === item.url)
              {
                fullProperties = item
              }
            })

            // add image
            setImages(
              images.concat([
                {
                  ...stageRef.current.getPointerPosition(),
                  src: dragUrl.current,
                  item: fullProperties,
                  id: images.length++
                }
              ])
            );
          }

          if(dragDropHelper.current.tagName === "P")
          { 
            setTextos(
              textos.concat([
                {
                  ...stageRef.current.getPointerPosition(),                  
                  id: textos.length++,
                  text: "myyy nigga",
                  scaleX: 2,
                  scaleY: 2,
                }
              ])
            );            
          }          
        }}
        onDragOver={e => e.preventDefault()}
      >
    <Stage
      width={900}
      height={window.innerHeight}
      ref={stageRef}
      onMouseDown={e => {
        const clickedOnEmpty = e.target === e.target.getStage();
        if (clickedOnEmpty) {
          selectPersonajeShape(null);
          selectObjectShape(null);
          selectTextShape(null);
        }
      }}
    >
      <Layer ref={layerRef}>
        {textos.map((item, i) => {                
              return (
                <TextTransform 
                      key={i++}                                            
                      stage={stageRef}
                      layer={layerRef}
                      shapeProps={item}                      
                      isSelected={item.id === textSelectedId}
                      onSelect={() => {
                        selectTextShape(item.id);
                        selectPersonajeShape(null);
                        selectObjectShape(null);          
                      }}
                      onSave={(attrs) => {   
                        // let temp = transformToSave(attrs, itemsBrazosPiernas)
                        // setItemsBrazosPiernas(temp) 
                        // onSaveShape(attrs);
                      }}  
                ></TextTransform>
               )
        })}


        {images.map((item, i) => {                
              return (
                <URLImageTransformObjeto
                      key={i++}
                      x={item.x}
                      y={item.y}                  
                      shapeProps={item.item}                      
                      isSelected={item.id === objectSelectedId}
                      onSelect={() => {
                        selectObjectShape(item.id);
                        selectPersonajeShape(null);          
                        selectTextShape(null);
                      }}
                      onSave={(attrs) => {   
                        // let temp = transformToSave(attrs, itemsBrazosPiernas)
                        // setItemsBrazosPiernas(temp) 
                        // onSaveShape(attrs);
                      }}                        
                    />
                )
            })}

        {/* PERSONAJES */}
          {editarPagina.personajes.map((itemCabezaGroup) => { 
            return(
            itemCabezaGroup.map((element) => {                             
              if(element.category === 10){                 
                  return (                              
                    <GroupImageTransform
                            key={element.id}
                            shapeProps={itemCabezaGroup}
                            isSelected={element.id === personajeSelectedId}
                            onSelect={() => {
                              selectPersonajeShape(element.id);                              
                              selectObjectShape(null);
                              selectTextShape(null);
                            }}                     
                          />  
                    )
                }
              }))            
            }
          )}

        {/* OBJETOS */}        
         {/* {editarPagina.galeria.map((item, i) => {                     
                  return (
                  <URLImageTransformObjeto
                      key={i++}
                      shapeProps={item}                      
                      isSelected={item.id === selectedId}
                      onSelect={() => {
                        selectShape(item.id);                         
                      }}
                      onSave={(attrs) => {   
                        // let temp = transformToSave(attrs, itemsBrazosPiernas)
                        // setItemsBrazosPiernas(temp) 
                        // onSaveShape(attrs);
                      }}                        
                    />
                )}
            )}  */}
      </Layer>
    </Stage>
      </div>
    </div>
  
  );
}

EditarPagina.propTypes = {
  dispatch: PropTypes.func.isRequired,
  editarPagina: PropTypes.object,  
  getPaginaLocal: PropTypes.func,
  getGaleriaLocal: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  editarPagina: makeSelectEditarPagina(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPaginaLocal: evt => dispatch(getPaginaAction()),
    getGaleriaLocal: evt => dispatch(getGaleriaAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(EditarPagina);
