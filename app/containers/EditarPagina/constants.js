/*
 *
 * EditarPagina constants
 *
 */

export const DEFAULT_ACTION = 'app/EditarPagina/DEFAULT_ACTION';
export const GETPAGINA_ACTION = 'app/EditarPagina/GETPAGINA_ACTION';
export const GETGALERIA_ACTION = 'app/EditarPagina/GETGALERIA_ACTION';

export const LOAD_PERSONAJES = 'boilerplate/EditarPagina/LOAD_PERSONAJES';
export const LOAD_GALERIA = 'boilerplate/EditarPagina/LOAD_GALERIA';

