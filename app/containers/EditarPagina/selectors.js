import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the editarPagina state domain
 */

const selectEditarPaginaDomain = state => state.editarPagina || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by EditarPagina
 */

const makeSelectEditarPagina = () =>
  createSelector(
    selectEditarPaginaDomain,
    substate => substate,
  );

export default makeSelectEditarPagina;
export { selectEditarPaginaDomain };
