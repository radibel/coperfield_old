/*
 * EditarPagina Messages
 *
 * This contains all the text for the EditarPagina container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.EditarPagina';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the EditarPagina container!',
  },
});
