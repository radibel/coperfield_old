import { takeLatest, take, call, put, select } from 'redux-saga/effects';
import { GETPAGINA_ACTION, LOAD_PERSONAJES, GETGALERIA_ACTION } from 'containers/EditarPagina/constants';
import { defaultAction, loadPersonajes, loadGaleria } from 'containers/EditarPagina/actions';
import request from 'utils/request';

// Individual exports for testing
export default function* editarPaginaSaga() {
  yield takeLatest(GETPAGINA_ACTION, getPersonajeCompleto_saga); 
  yield takeLatest(GETGALERIA_ACTION, getGaleria_saga); 
}

export function* getPersonajeCompleto_saga() {    

/*   const personajes = [  
    [{ 
      "category": 10,
      "id": "Cabeza Group default",
      "scaleX":0.5,
      "scaleY":0.5,
      "x": 400,
      "y": 500,
      "offsetX": 400,
      "offsetY": 500,
      "selected": true
    }, 
    { 
      "category": 11,
      "id": "cara contorno",          
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 400,
      "y": 200,        
      "url": "https://i.imgur.com/lh7rUC8.png",
      "selected": true
    },
    { 
      "category": 1,
      "id": "cuerpo flaco",
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 400,
      "y": 450,
      "url": "https://i.imgur.com/QH0R7sH.png",
      "selected": true
    },
    { 
      "category": 3,
      "id": "expresion funky",    
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 400,
      "y": 200,
      "selected": true,      
      "url": "https://i.imgur.com/CoQcC6O.png"
    },
    { 
      "category": 4,
      "id": "peinado niopa",    
      "scaleX":0.34,
      "scaleY":0.34,
      "x": 355,
      "y": 160,  
      "selected": true,
      "url": "https://i.imgur.com/H2b0kbf.png"      
    },
    { 
      "category": 9,
      "id": "pierna izquierdo default",    
      "scaleX":0.12,
      "scaleY":0.12,
      "offsetX":500,
      "offsetY":50,
      "x": 480,
      "y": 700, 
      "rotation": 100, 
      "selected": true,
      "url": "https://i.imgur.com/hMlP9tV.png"
    },
    { 
      "category": 9,
      "id": "pierna derecho default",    
      "scaleX":0.12,
      "scaleY":0.12,
      "offsetX":500,
      "offsetY":50,
      "x": 570,
      "y": 700,  
      "rotation": 270, 
      "selected": true,
      "url": "https://i.imgur.com/c8a6Nba.png"
    },
    { 
      "category": 2,
      "id": "brazo izquierdo default",    
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 495,
      "y": 490,
      "offsetX": 500,
      "offsetY": 0,
      "rotation": 90,
      "selected": true,
      "url": "https://i.imgur.com/dMJDkcR.png"
    },
    { 
      "category": 2,
      "id": "brazo derecho default",    
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 555,
      "y": 490,
      "offsetX": 500,
      "offsetY": 0,
      "rotation": 270,
      "selected": true,
      "url": "https://i.imgur.com/iNhToh2.png"
    }],
    [{ 
      "category": 10,
      "id": "Cabeza Group default2",
      "scaleX":0.5,
      "scaleY":0.5,
      "x": 700,
      "y": 400,
      "offsetX": 400,
      "offsetY": 500,
      "rotation": 25,
      "selected": true
    },  
    { 
      "category": 11,
      "id": "cara contorno2",          
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 400,
      "y": 200,        
      "url": "https://i.imgur.com/lh7rUC8.png",
      "selected": true
    },
    { 
      "category": 1,
      "id": "cuerpo cerdo",
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 400,
      "y": 450,
      "url": "https://i.imgur.com/uU7aPlX.png",
      "selected": true
    },
    { 
      "category": 3,
      "id": "expresion enojado",    
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 400,
      "y": 200,
      "selected": true,      
      "url": "https://i.imgur.com/547XUoP.png"
    },
    { 
      "category": 4,
      "id": "peinado nigga",    
      "scaleX":0.34,
      "scaleY":0.34,
      "x": 355,
      "y": 160,  
      "selected": true,
      "url": "https://i.imgur.com/T7L0JhE.png"      
    },
    { 
      "category": 9,
      "id": "pierna izquierdo default",    
      "scaleX":0.12,
      "scaleY":0.12,
      "offsetX":500,
      "offsetY":50,
      "x": 480,
      "y": 700, 
      "rotation": 0, 
      "selected": true,
      "url": "https://i.imgur.com/hMlP9tV.png"
    },
    { 
      "category": 9,
      "id": "pierna derecho default",    
      "scaleX":0.12,
      "scaleY":0.12,
      "offsetX":500,
      "offsetY":50,
      "x": 570,
      "y": 700,  
      "rotation": 25, 
      "selected": true,
      "url": "https://i.imgur.com/c8a6Nba.png"
    },
    { 
      "category": 2,
      "id": "brazo izquierdo default",    
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 495,
      "y": 490,
      "offsetX": 500,
      "offsetY": 0,
      "rotation": 150,
      "selected": true,
      "url": "https://i.imgur.com/dMJDkcR.png"
    },
    { 
      "category": 2,
      "id": "brazo derecho default",    
      "scaleX":0.25,
      "scaleY":0.25,
      "x": 555,
      "y": 490,
      "offsetX": 500,
      "offsetY": 0,
      "rotation": 210,
      "selected": true,
      "url": "https://i.imgur.com/iNhToh2.png"
    }]
  ] */

  try {
    // Call our request helper (see 'utils/request')
    // const items = yield call(request, requestURL);
    // const items = JSON.stringify(personajeCompleto)
    
    const personajes = yield call(request, "http://localhost:3001/libros/1");
    
    yield put(loadPersonajes(personajes[0].detalle));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }  
}

export function* getGaleria_saga() {    

  const objetos = [  
    { 
      category: 6,
      id: 'nube',
      scaleX:.15,
      scaleY:.15,                  
      url: 'https://i.imgur.com/cczpSCB.png',
      selected: true
    }
  ];

  try {
    // Call our request helper (see 'utils/request')
    // const items = yield call(request, requestURL);
    // const items = JSON.stringify(personajeCompleto)
    
    yield put(loadGaleria(objetos));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }  
}