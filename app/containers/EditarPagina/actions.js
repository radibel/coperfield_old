/*
 *
 * EditarPagina actions
 *
 */

import { DEFAULT_ACTION, GETPAGINA_ACTION, LOAD_PERSONAJES, LOAD_GALERIA, GETGALERIA_ACTION } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getPaginaAction() {
  return {
    type: GETPAGINA_ACTION,  
  };
}

export function getGaleriaAction() {
  return {
    type: GETGALERIA_ACTION,  
  };
}

export function loadPersonajes(personajes) {
  return {
    type: LOAD_PERSONAJES,
    personajes
  };
}

export function loadGaleria(galeria) {
  return {
    type: LOAD_GALERIA,
    galeria
  };
}