import React from 'react'
import { Image, Transformer } from 'react-konva';
import useImage from 'use-image';

const URLImage = ({ shapeProps }) => {
    const [image] = useImage(shapeProps.url);
    const [props, setProps] = React.useState(shapeProps);

    const shapeRef = React.useRef();
    
    return (
      <React.Fragment>
        <Image
          {...props}          
          ref={shapeRef}          
          image={image}          
        />
      </React.Fragment>
    );
  };

  export default URLImage
