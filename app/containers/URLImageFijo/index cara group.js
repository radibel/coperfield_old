import React from 'react'
import { Image, Transformer } from 'react-konva';
import useImage from 'use-image';

const URLImage = ({ shapeProps, isSelected, onSelect, onChange }) => {
    const [image] = useImage(shapeProps.url);
    const [props, setProps] = React.useState(shapeProps);

    const shapeRef = React.useRef();
    const trRef = React.useRef();    

    React.useEffect(() => {                 
      if (isSelected) {        
        trRef.current.setNode(shapeRef.current);
        trRef.current.getLayer().batchDraw();        
      }
    }, [isSelected]); 

    return (
      <React.Fragment>
        <Image
          onClick={onSelect}
          offsetX={props.offsetX}
          offsetY={props.offsetY}
          {...shapeProps}
          ref={shapeRef}
          id={props.id}
          scaleX={props.scaleX}
          scaleY={props.scaleY}
          x={props.x}
          y={props.y}
          image={image}               
          // onTransform={e => {            
          //   const node = shapeRef.current;       
          //   node.position({x:props.x, y: props.y})            
          // }} 
          // onTransformEnd={e => {
          //   // transformer is changing scale of the node
          //   // and NOT its width or height
          //   // but in the store we have only width and height
          //   // to match the data better we will reset scale on transform end
          //   const node = shapeRef.current;
          //   // const scaleX = node.scaleX();
          //   // const scaleY = node.scaleY();

          //   // // we will reset it back
          //   // node.scaleX(.5);
          //   // node.scaleY(.5);                        
     
          //   onChange({
          //     ...shapeProps,
          //     x: node.x(),
          //     y: node.y(),              
          //     // set minimal value
          //     width: Math.max(5, node.width() * node.scaleX),
          //     height: Math.max(node.height() * node.scaleY)              
          //   });
          // }}
        />      
    {isSelected && (
          <Transformer          
            rotateAnchorOffset={50}
            resizeEnabled={false}
            anchorCornerRadius={4}            
            ref={trRef}
            boundBoxFunc={(oldBox, newBox) => {
              // limit resize
              if (newBox.width < 5 || newBox.height < 5) {
                return oldBox;
              }
              return newBox;
            }}
          />
        )}
        {/* <Rect
          offsetY={15}
          onClick={onSelect}
          ref={shapeRef}
          {...shapeProps}
          onTransform={e => {
            const node = shapeRef.current;          
            node.position({x:500, y: 300})
          }}          
          onTransformEnd={e => {
            // transformer is changing scale of the node
            // and NOT its width or height
            // but in the store we have only width and height
            // to match the data better we will reset scale on transform end
            const node = shapeRef.current;
            const scaleX = node.scaleX();
            const scaleY = node.scaleY();

            // we will reset it back
            node.scaleX(1);
            node.scaleY(1);                        
     
            onChange({
              ...shapeProps,
              x: node.x(),
              y: node.y(),              
              // set minimal value
              width: Math.max(5, node.width() * scaleX),
              height: Math.max(node.height() * scaleY)              
            });
          }}
        /> */}
        
      </React.Fragment>
    );
  };

  export default URLImage
