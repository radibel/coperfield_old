import { GETALLCATEGORIES_ACTION, SAVE_ACTION, SET_CUERPO_SELECTED, GETPERSONAJECOMPLETO_ACTION, LOAD_PERSONAJECOMPLETO_ACTION } from './constants';
import { LOAD_ALLCATEGORIES, LOAD_REPOS_SUCCESS, LOAD_REPOS_ERROR } from './constants';

export function getAllCategories() {  
  return {
    type: GETALLCATEGORIES_ACTION
  };
}

export function loadAllCategories(categories) {  
  return {
    type: LOAD_ALLCATEGORIES,
    categories
  };
}

export function saveShapeAction(attrs) {  
  return {
    type: SAVE_ACTION,
    attrs
  };
}

export function setCuerpoSelected(id) {  
  return {
    type: SET_CUERPO_SELECTED,    
    id
  };
}

export function getPersonajeCompleto() {  
  return {
    type: GETPERSONAJECOMPLETO_ACTION
  };
}

export function loadPersonajeCompleto(items) {  
  return {
    type: LOAD_PERSONAJECOMPLETO_ACTION,
    items
  };
}