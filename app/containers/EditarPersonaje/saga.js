import { takeLatest, take, call, put, select } from 'redux-saga/effects';
import { GETALLCATEGORIES_ACTION, SAVE_ACTION, GETPERSONAJECOMPLETO_ACTION } from 'containers/EditarPersonaje/constants';
import { getAllCategories, loadAllCategories, loadPersonajeCompleto } from 'containers/EditarPersonaje/actions';
import request from 'utils/request';

import { LOAD_REPOS } from 'containers/EditarPersonaje/constants';
import { reposLoaded, repoLoadingError } from 'containers/EditarPersonaje/actions';
import { setCuerpoSelected } from './actions';

// Individual exports for testing
export default function* editarPersonajeSaga() {
  yield takeLatest(GETALLCATEGORIES_ACTION, getAllCategories_saga);
  yield takeLatest(GETPERSONAJECOMPLETO_ACTION, getPersonajeCompleto_saga);  
}

export function* getPersonajeCompleto_saga() {
  // Select username from store
  // const username = yield select(makeSelectUsername());
  // const username = '@anai.dominick'
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  console.log("saga")
  // const requestURL = "http://192.168.1.103:8080/item/all"
//   cuerpos
const requestURL = "http://localhost:8080/item/get/category/id/1"
//   BRAZOS
  // const requestURL = "http://localhost:8080/item/get/category/id/2"
// expresion
  const byCatItems_url3 = "http://localhost:8080/item/get/category/id/3"
//   peinados
  const byCatItems_url4 = "http://localhost:8080/item/get/category/id/4"

  const personajeCompleto = [  
    // { 
    //   category: 10,
    //   id: 'Cabeza Group default',
    //   scaleX:.5,
    //   scaleY:.5,
    //   x: 400,
    //   y: 200,
    //   offsetX: 0,
    //   offsetY: 0,
    //   selected: true,
    //   url: null,
    // },  
    { 
      category: 11,
      id: 'cara contorno',          
      scaleX:.22,
      scaleY:.22,
      x: 415,
      y: 230,
      url: 'https://i.imgur.com/lh7rUC8.png',
      selected: true,
    },
    { 
      category: 3,
      id: 'expresion feliz',    
      scaleX:.25,
      scaleY:.25,
      x: 400,
      y: 220,
      selected: true,      
      url: 'https://i.imgur.com/0goTsaU.png'
    },
    { 
      category: 4,
      id: 'peinado niopa',    
      scaleX:.30,
      scaleY:.30,
      x: 375,
      y: 190,  
      selected: true,
      url: 'https://i.imgur.com/H2b0kbf.png'      
    },
    { 
      category: 1,
      id: "cuerpo flaco",
      scaleX:.25,
      scaleY:.25,
      x: 400,
      y: 450,
      url: "https://i.imgur.com/QH0R7sH.png",
      selected: true,
    },
    { 
      category: 9,
      id: 'pierna izquierdo default',    
      scaleX:.12,
      scaleY:.12,
      offsetX:500,
      offsetY:50,
      x: 480,
      y: 700,  
      selected: true,
      url: 'https://i.imgur.com/hMlP9tV.png'
    },
    { 
      category: 12,
      id: 'pierna derecho default',    
      scaleX:.12,
      scaleY:.12,
      offsetX:500,
      offsetY:50,
      x: 570,
      y: 700,  
      selected: true,
      url: 'https://i.imgur.com/c8a6Nba.png'
    },
    { 
      category: 2,
      id: 'brazo izquierdo default',    
      scaleX:.25,
      scaleY:.25,
      x: 495,
      y: 490,
      offsetX: 500,
      offsetY: 0,
      rotation: 90,
      selected: true,
      url: 'https://i.imgur.com/dMJDkcR.png'
    },
    { 
      category: 13,
      id: 'brazo derecho default',    
      scaleX:.25,
      scaleY:.25,
      x: 555,
      y: 490,
      offsetX: 500,
      offsetY: 0,
      rotation: 270,
      selected: true,
      url: 'https://i.imgur.com/iNhToh2.png'
    }
  ];

  try {
    // Call our request helper (see 'utils/request')
    // const items = yield call(request, requestURL);
    // const items = JSON.stringify(personajeCompleto)
    const items = personajeCompleto
    console.log(items)
    yield put(loadPersonajeCompleto(items));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }  
}


export function* getAllCategories_saga() {  
  // Select username from store
  // const username = yield select(makeSelectUsername());
  // const username = '@anai.dominick'
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  const requestURL = "http://192.168.1.103:8080/item/all"
//   cuerpos
  const requestURL1 = "http://localhost:8080/item/get/category/id/1"
//   brazos
  const requestURL2 = "http://localhost:8080/item/get/category/id/2"
// expresion
  const byCatItems_url3 = "http://localhost:8080/item/get/category/id/3"
//   peinados
  const byCatItems_url4 = "http://localhost:8080/item/get/category/id/4"

  const allCategories = [
    { 
      category: 10,
      id: 'Cabeza Group default',
      scaleX:.5,
      scaleY:.5,
      x: 400,
      y: 200,
      offsetX: 0,
      offsetY: 0,
      selected: false
    },  
    { 
      category: 11,
      id: 'cara contorno',          
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,        
      url: 'https://i.imgur.com/lh7rUC8.png',
      selected: false,
    },
    { 
      category: 1,
      id: "cuerpo flaco",
      scaleX:".25",
      scaleY:".25",
      x: 400,
      y: 450,
      url: "https://i.imgur.com/QH0R7sH.png",
      selected: false,
    },
    { 
      category: 1,
      id: "cuerpo cerdo",
      scaleX:.25,
      scaleY:.25,
      x: 400,
      y: 450,
      url: "https://i.imgur.com/uU7aPlX.png",      
      selected: false,
    },
    { 
      category: 3,
      id: 'expresion feliz',    
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,
      selected: false,      
      url: 'https://i.imgur.com/0goTsaU.png'
       
    },
    { 
      category: 3,
      id: 'expresion enojado',    
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,
      selected: false,      
      url: 'https://i.imgur.com/547XUoP.png'
    },
    { 
      category: 3,
      id: 'expresion japanis',    
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,
      selected: false,      
      url: 'https://i.imgur.com/iOYMC9G.png'
    },
    { 
      category: 3,
      id: 'expresion funky',    
      scaleX:.5,
      scaleY:.5,
      x: 0,
      y: 0,
      selected: false,      
      url: 'https://i.imgur.com/CoQcC6O.png'
    },      
    { 
      category: 4,
      id: 'peinado niopa',    
      scaleX:.68,
      scaleY:.68,
      x: -90,
      y: -100,  
      selected: false,
      url: 'https://i.imgur.com/H2b0kbf.png'      
    },
    { 
      category: 4,
      id: 'peinado nigga',    
      scaleX:.68,
      scaleY:.68,
      x: -90,
      y: -100,  
      selected: false,      
      url: 'https://i.imgur.com/T7L0JhE.png'      
    },
    { 
      category: 4,
      id: 'peinado default',    
      scaleX:.68,
      scaleY:.68,
      x: -90,
      y: -100,  
      selected: false,      
      url: 'https://i.imgur.com/m6L31Hx.png'
    },
    { 
      category: 9,
      id: 'pierna izquierdo default',    
      scaleX:.12,
      scaleY:.12,
      offsetX:500,
      offsetY:50,
      x: 480,
      y: 700,  
      selected: false,
      url: 'https://i.imgur.com/hMlP9tV.png'
    },
    { 
      category: 9,
      id: 'pierna derecho default',    
      scaleX:.12,
      scaleY:.12,
      offsetX:500,
      offsetY:50,
      x: 570,
      y: 700,  
      selected: false,
      url: 'https://i.imgur.com/c8a6Nba.png',
      master: true
    },
    { 
      category: 2,
      id: 'brazo izquierdo default',    
      scaleX:.25,
      scaleY:.25,
      x: 495,
      y: 490,
      offsetX: 500,
      offsetY: 0,
      rotation: 90,
      selected: false,
      url: 'https://i.imgur.com/dMJDkcR.png'      
    },
    { 
      category: 2,
      id: 'brazo derecho default',    
      scaleX:.25,
      scaleY:.25,
      x: 555,
      y: 490,
      offsetX: 500,
      offsetY: 0,
      rotation: 270,
      selected: false,
      url: 'https://i.imgur.com/iNhToh2.png',
      master: true
    }
  ];

  
  try {
    // Call our request helper (see 'utils/request')
    // const items = yield call(request, requestURL);    
    const categories = allCategories
    console.log(categories)
    yield put(loadAllCategories(categories));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }  
}

export function* saveShape() {
  // Select username from store
  // const username = yield select(makeSelectUsername());
  // const username = '@anai.dominick'
  // const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;
  console.log("saga save")
  const requestURL = "http://192.168.1.103:8080/item/all"

  try {
    
    //SAVE SHAPE HERE
    const result = yield call(request, requestURL);    
    // yield put(loadAll(items));
  } catch (err) {
    console.log("error en saga: " + err)
    // yield put(repoLoadingError(err));
  }
}







// const initialCabeza = [  
//   { 
//     id: 'cabeza',    
//     scaleX:.5,
//     scaleY:.5,
//     x: 0,
//     y: 0,        
//     url: 'https://i.imgur.com/lh7rUC8.png'
//   },
//   { 
//     id: 'cara',    
//     scaleX:.5,
//     scaleY:.5,
//     x: 0,
//     y: 0,
//     //url: 'https://i.imgur.com/CoQcC6O.png'
//     //url: 'https://i.imgur.com/547XUoP.png'
//     // url: 'https://i.imgur.com/iOYMC9G.png'
//      url: 'https://i.imgur.com/0goTsaU.png'
//   },
//   // { 
//   //   id: 'cuerpo',    
//   //   scaleX:.5,
//   //   scaleY:.5,
//   //   x: 0,
//   //   y: 500,
//   //   // url: 'https://i.imgur.com/QH0R7sH.png',
//   //   url: 'https://i.imgur.com/uU7aPlX.png'
//   // },
//   { 
//     id: 'peinado',    
//     scaleX:.68,
//     scaleY:.68,
//     x: -90,
//     y: -100,  
//     url: 'https://i.imgur.com/H2b0kbf.png'
//     //url: 'https://i.imgur.com/T7L0JhE.png'
//     //url: 'https://i.imgur.com/m6L31Hx.png'
//   }
// ];

// const initialCuerpo = [ 
//   { 
//     id: 'cuerpo',    
//     scaleX:.25,
//     scaleY:.25,
//     x: 400,
//     y: 450,
//     url: 'https://i.imgur.com/QH0R7sH.png',
//     // url: 'https://i.imgur.com/uU7aPlX.png'
//   },
// ];

// const initialCabezaGroup = [
//   { 
//     id: 'cabezota',
//     scaleX:.5,
//     scaleY:.5,
//     x: 400,
//     y: 200,
//     offsetX: 0,
//     offsetY: 0,
//   }
// ];

// const initialBrazosPiernas = [    
//   { 
//     id: 'pierna izquierdo',    
//     scaleX:.12,
//     scaleY:.12,
//     offsetX:500,
//     offsetY:50,
//     x: 480,
//     y: 700,  
//     url: 'https://i.imgur.com/hMlP9tV.png'
//   },
//   { 
//     id: 'pierna derecho',    
//     scaleX:.12,
//     scaleY:.12,
//     offsetX:500,
//     offsetY:50,
//     x: 570,
//     y: 700,  
//     url: 'https://i.imgur.com/c8a6Nba.png'
//   },
//   { 
//     id: 'brazo izquierdo',    
//     scaleX:.25,
//     scaleY:.25,
//     x: 495,
//     y: 490,
//     offsetX: 500,
//     offsetY: 0,
//     rotation: 90,
//     url: 'https://i.imgur.com/dMJDkcR.png'
//   },
//   { 
//     id: 'brazo derecho',    
//     scaleX:.25,
//     scaleY:.25,
//     x: 555,
//     y: 490,
//     offsetX: 500,
//     offsetY: 0,
//     rotation: 270,
//     url: 'https://i.imgur.com/iNhToh2.png'
//   }
// ];