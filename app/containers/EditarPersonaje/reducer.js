import produce from 'immer';
import { GETALLCATEGORIES_ACTION, LOAD_ALLCATEGORIES, SET_CUERPO_SELECTED, GETPERSONAJECOMPLETO_ACTION, LOAD_PERSONAJECOMPLETO_ACTION } from './constants';
import { element } from 'prop-types';

export const initialState = {
  loading: false,
  error: false,  
  brazosPiernas: [],
  peinados: [],
  cuerpos: [],
  expresiones: [], 
  items: [],  
  categories: [],
  id: 0
};

/* eslint-disable default-case, no-param-reassign */
const editarPersonajeReducer = (state = initialState, action) =>
produce(state, draft => {
  let elements = []
  let elementsPersonaje = []
  switch (action.type) {
    case GETALLCATEGORIES_ACTION:      
      draft.loading = true;
      break;
    case LOAD_PERSONAJECOMPLETO_ACTION:      
      draft.loading = true;            
      elementsPersonaje = action.items             
      elementsPersonaje.forEach(element => {                       
        element.scaleX = parseFloat(element.scaleX) 
        element.scaleY = parseFloat(element.scaleY)                                
      })          
      draft.items = elementsPersonaje
      draft.loading = false; 
      break;
    case LOAD_ALLCATEGORIES:      
      draft.loading = true;
      let num = 0;
      let tempBrazosPiernas = []
      // action.categories.forEach(element => {               
      //   elements.push(JSON.parse(element.properties))
      // });        
      elements = action.categories
      elements.forEach(element => {                       
        element.scaleX = parseFloat(element.scaleX) 
        element.scaleY = parseFloat(element.scaleY)                
        element.index = num++
        element.selected = (element.selected == 'true') ? true : false    

        if(element.category === 2 && element.master)
        {          
          tempBrazosPiernas.push(element)
        }
      })

      draft.brazosPiernas = tempBrazosPiernas
      draft.categories = elements      
      draft.loading = false;
      break;   
    case SET_CUERPO_SELECTED:
      draft.loading = true;      
      let tempNewItems = [];      
      let tempItems = [];
      let tempEleccion;
      
      tempEleccion = state.categories.find(x => x.id === action.id)
      tempEleccion.selected = true
      let category = tempEleccion.category      
      tempItems = state.items;

      state.categories.forEach(element => {
        if(element.id === action.id){
          element.selected = true         
        }
        else 
        {
          if(element.category === category)
            element.selected = false
        }          
      });

      tempItems.forEach(personajeElement => {
        if(personajeElement.category === category)
        {
          if(personajeElement.category === 2)
          {
            tempEleccion.selected = true
            tempNewItems.push(tempEleccion)
          }else
          {
            tempEleccion.selected = true
            tempNewItems.push(tempEleccion)
          }          
        }else{
          tempNewItems.push(personajeElement)
        }
      });
    
      draft.categories = state.categories
      draft.items = tempNewItems
      draft.loading = false;      
    break;
   }
});

export default editarPersonajeReducer;
