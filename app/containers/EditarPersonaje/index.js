import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectEditarPersonaje from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { Row, Col, ListGroup, Button } from "react-bootstrap";

import { isEmpty, isNull } from 'lodash';
import { getAllCategories, setCuerpoSelected, getPersonajeCompleto  } from './actions';
import Personaje from '../Personaje'


export function EditarPersonaje({
  editarPersonaje,
  personaje,
  startCategoriesCall,
  setCuerpoSelectedStart,
  getPersonajeDefault,
}) {
  useInjectReducer({ key: 'editarPersonaje', reducer });
  useInjectSaga({ key: 'editarPersonaje', saga });

  const [itemsCuerpos, setItemsCuerpos] = React.useState(null);  
  
   React.useEffect(() => {    
    getPersonajeDefault();
    startCategoriesCall();    
  }, []); 
 
  
// const temp = (indexSelected) => {  
//   setCuerpoSelectedStart(indexSelected) 
// }

  return (
    <div>
      <Col lg={3} md={3} className="columna">                 
                <ListGroup>
                    <ListGroup.Item variant='info' >CUERPOS</ListGroup.Item>
                    {editarPersonaje.categories.map(item => {
                      if(item.category === 1){
                        return(
                          <ListGroup.Item key={item.id} variant='light' action 
                        onClick={()=> setCuerpoSelectedStart(item.id)}>{item.id}</ListGroup.Item>
                          )}
                        }) 
                    } 
                </ListGroup>                    
            </Col>
            <Col lg={3} md={3} className="columna">
            <ListGroup>
                <ListGroup.Item variant='info'>BRAZOS</ListGroup.Item>
                {editarPersonaje.brazosPiernas.map(item => {
                      if(item.category === 2){
                        return(
                          <ListGroup.Item key={item.id} variant='light' action 
                        onClick={()=> setCuerpoSelectedStart(item.id)}>{item.id}</ListGroup.Item>
                          )}
                        }) 
                    } 
                </ListGroup>
            </Col>
            <Col lg={3} md={3} className="columna">
            <ListGroup>  
                <ListGroup.Item variant='info'>EXPRESION</ListGroup.Item>
                {editarPersonaje.categories.map(item => {
                      if(item.category === 3){
                        return(
                          <ListGroup.Item key={item.id} variant='light' action 
                        onClick={()=> setCuerpoSelectedStart(item.id)}>{item.id}</ListGroup.Item>
                          )}
                        }) 
                    } 
                </ListGroup>
            </Col>
            <Col lg={3} md={3} className="columna">
            <ListGroup>  
                <ListGroup.Item variant='info'>PEINADOS</ListGroup.Item>
                {editarPersonaje.categories.map(item => {
                      if(item.category === 4){
                        return(
                          <ListGroup.Item key={item.id} variant='light' action 
                        onClick={()=> setCuerpoSelectedStart(item.id)}>{item.id}</ListGroup.Item>
                          )}
                        }) 
                    } 
                </ListGroup>
            </Col>
           
            <Personaje></Personaje>            
    </div>
  );
}

EditarPersonaje.propTypes = {
  dispatch: PropTypes.func.isRequired,
  startCategoriesCall: PropTypes.func,
  setCuerpoSelectedStart: PropTypes.func,  
  getPersonajeDefault: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  editarPersonaje: makeSelectEditarPersonaje(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    startCategoriesCall: evt => dispatch(getAllCategories()),
    setCuerpoSelectedStart: evt => dispatch(setCuerpoSelected(evt)),    
    getPersonajeDefault: evt => dispatch(getPersonajeCompleto()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(EditarPersonaje);
