import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the editarPersonaje state domain
 */

const selectEditarPersonajeDomain = state =>
  state.editarPersonaje || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by EditarPersonaje
 */

const makeSelectEditarPersonaje = () =>
  createSelector(
    selectEditarPersonajeDomain,
    substate => substate,
  );

export default makeSelectEditarPersonaje;
export { selectEditarPersonajeDomain };
