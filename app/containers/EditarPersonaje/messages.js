/*
 * EditarPersonaje Messages
 *
 * This contains all the text for the EditarPersonaje container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.EditarPersonaje';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the EditarPersonaje container!',
  },
});
