/*
 *
 * EditarPersonaje constants
 *
 */

export const GETALLCATEGORIES_ACTION = 'app/EditarPersonaje/GETALLCATEGORIES_ACTION';
export const SET_CUERPO_SELECTED = 'app/EditarPersonaje/SET_CUERPO_SELECTED';
export const GETPERSONAJECOMPLETO_ACTION = 'app/EditarPersonaje/GETPERSONAJECOMPLETO_ACTION';

export const LOAD_PERSONAJECOMPLETO_ACTION = 'boilerplate/EditarPersonaje/LOAD_PERSONAJECOMPLETO_ACTION';
export const LOAD_ALLCATEGORIES = 'boilerplate/EditarPersonaje/LOAD_ALLCATEGORIES';
export const LOAD_REPOS_SUCCESS = 'boilerplate/EditarPersonaje/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS_ERROR = 'boilerplate/EditarPersonaje/LOAD_REPOS_ERROR';
