import React from 'react'
import { Image, Transformer } from 'react-konva';
import useImage from 'use-image';

const URLImageTransformObjeto = ({ shapeProps, isSelected, onSelect, onSave, x, y }) => {
    const [url] = useImage(shapeProps.url);
    const [props, setProps] = React.useState(shapeProps);    

    const shapeRef = React.useRef();
    const trRef = React.useRef();

    React.useEffect(() => {      
      if (isSelected) {
        // let temp = shapeRef.current.zIndex()
        // shapeRef.current.zIndex(1)
        // shapeRef.current.batchDraw()
        trRef.current.setNode(shapeRef.current);
        trRef.current.getLayer().batchDraw();
        
      }
    }, [isSelected]);

    return (
      <React.Fragment>
        <Image
          x={x}
          y={y}
          onClick={onSelect}
          draggable={true}
          {...props}
          ref={shapeRef}          
          image={url}          
          onTransformEnd={e => {            
            // onSave(shapeRef.current.attrs)
          }}                     
        />           
    {isSelected && (
          <Transformer            
            rotateAnchorOffset={50}            
            anchorSize={20}
            anchorFill={"pink"}
            anchorStroke={"black"}            
            borderEnabled={true}
            resizeEnabled={true}
            anchorCornerRadius={30}            
            ref={trRef}
            boundBoxFunc={(oldBox, newBox) => {
              // limit resize
              if (newBox.width < 5 || newBox.height < 5) {
                return oldBox;
              }
              return newBox;
            }}
          />
        )}
      </React.Fragment>
    );
  };

  export default URLImageTransformObjeto
