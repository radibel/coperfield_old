import React from 'react'
import { Image, Transformer } from 'react-konva';
import useImage from 'use-image';

const URLImageTransform = ({ shapeProps, isSelected, onSelect, onSave }) => {
    const [image] = useImage(shapeProps.url);
    const [props, setProps] = React.useState(shapeProps);    

    const shapeRef = React.useRef();
    const trRef = React.useRef();

    React.useEffect(() => {      
      if (isSelected) {
        trRef.current.setNode(shapeRef.current);
        trRef.current.getLayer().batchDraw();        
      }
    }, [isSelected]);

    return (
      <React.Fragment>
        <Image
          onClick={onSelect}
          {...props}
          ref={shapeRef}          
          image={image}
          onTransform={e => {            
            const node = shapeRef.current;       
            node.position({x:props.x, y: props.y})            
          }}
          onTransformEnd={e => {            
            onSave(shapeRef.current.attrs)
          }} 
        />      
    {isSelected && (
          <Transformer            
            rotateAnchorOffset={100}
            rotateAnchorOffset={20}            
            anchorSize={20}
            anchorFill={"pink"}
            anchorStroke={"black"}            
            borderEnabled={true}
            resizeEnabled={false}
            anchorCornerRadius={30}            
            ref={trRef}
            boundBoxFunc={(oldBox, newBox) => {
              // limit resize
              if (newBox.width < 5 || newBox.height < 5) {
                return oldBox;
              }
              return newBox;
            }}
          />
        )}
      </React.Fragment>
    );
  };

  export default URLImageTransform
